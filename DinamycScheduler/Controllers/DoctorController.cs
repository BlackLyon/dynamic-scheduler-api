﻿using System;
using DinamycScheduler.Model;
using DinamycScheduler.Repository.Doctor;
using Microsoft.AspNetCore.Mvc;

namespace DinamycScheduler
{
    [ApiController]
    [Route("[controller]")]
    public class DoctorController : Controller
    {
        private readonly IDoctorRepository doctorRepository;

        public DoctorController(IDoctorRepository doctorRepository)
        {
            this.doctorRepository = doctorRepository;
        }

        [HttpPost("turn/accept")]
        public IActionResult AcceptNextTurn([FromBody] RequestDto request)
        {
            var doctorId = request.Id;
            var time = new TimeSpan(request.Hour.Hours, request.Hour.Minutes, 0);
            var result = doctorRepository.AcceptNextTurn(doctorId, time);
            return Ok(result);
        }

        [HttpPost("turn/emergency/accept")]
        public IActionResult AcceptEmergencyTurn([FromBody] RequestDto request)
        {
            var doctorId = request.Id;
            var time = new TimeSpan(request.Hour.Hours, request.Hour.Minutes, 0);
            var result = doctorRepository.AcceptEmergencyTurn(doctorId, time);
            return Ok(result);
        }

        [HttpPost("session/extend")]
        public IActionResult ExtendSession([FromBody] int sessionId)
        {
            var result = doctorRepository.ExtendSession(sessionId);
            return Ok(result);
        }

        [HttpPost("session/end")]
        public IActionResult SaveSessionEndTime([FromBody] RequestDto request)
        {
            var sessionId = request.Id;
            var time = new TimeSpan(request.Hour.Hours, request.Hour.Minutes, 0);
            var result = doctorRepository.EndSession(sessionId, time);
            return Ok(result);
        }
    }
}
