﻿using System;
using DinamycScheduler.Model;
using DinamycScheduler.Repository.Patient;
using Microsoft.AspNetCore.Mvc;

namespace DinamycScheduler
{
    [ApiController]
    [Route("[controller]")]
    public class PatientController : Controller
    {
        private readonly IPatientRepository patientRepository;

        public PatientController(IPatientRepository patientRepository)
        {
            this.patientRepository = patientRepository;
        }

        [HttpGet("turn/{id}")]
        public IActionResult GetAsignedTurn([FromRoute] int id)
        {
            var assignedTurn = patientRepository.GetAssignedTurn(id);
            if (assignedTurn is null) return NotFound();
            return Ok(assignedTurn);
        }
       
        [HttpPost("turn/cancel")]
        public IActionResult CancelNextTurn([FromBody] int userId)
        {
            var result = patientRepository.CancelTurnAndAssignToNext(userId);
            if (result == TimeSpan.Zero) return NotFound();
            return Ok(result);
        }

        [HttpPost("turn/accept")]
        public IActionResult AcceptTurn([FromBody] RequestDto body)
        {
            var turnHour = body.Hour;
            var time = new TimeSpan(turnHour.Hours, turnHour.Minutes, 0);
            var result = patientRepository.AcceptNextAvailableTurn(body.Id, time);
            return Ok(result);
        }

        [HttpGet("turn/available/{userId}")]
        public IActionResult GetAvailableTurnHour([FromRoute] int userId)
        {
            var availableHour = patientRepository.GetAvailableTurnHour(userId);
            if (availableHour == TimeSpan.Zero) return NotFound();
            return Ok(availableHour);
        }
    }
}
