﻿using DinamycScheduler.Model;
using DinamycScheduler.Repository;
using Microsoft.AspNetCore.Mvc;

namespace DinamycScheduler.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : Controller
    {
        private readonly IUserRepository userRepository;

        public LoginController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpPost]
        public IActionResult Login([FromBody] Credentials credentials)
        {
            var user = this.userRepository.Login(credentials.DNI, credentials.Password);
            if (user is null) return NotFound();
            return Ok(user);
        }
    }
}