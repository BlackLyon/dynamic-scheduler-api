﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace DinamycScheduler
{
    public static class SwaggerConfiguration
    {
        private const string EndpointName = "API v2";
        private const string EndpointUrl = "/swagger/v2/swagger.json";
        private const string DocNameV2 = "v2";
        private const string DocInfoTitle = "My Api";
        private const string DocInfoVersion = "v2";
        private const string DocInfoDescription = "Doc of my Api";

        public static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc(
                    DocNameV2,
                    new OpenApiInfo
                    {
                        Title = DocInfoTitle,
                        Version = DocInfoVersion,
                        Description = DocInfoDescription
                    });
            });
        }

        public static void UseSwaggerUi(this IApplicationBuilder app)
        {
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(config => { config.SwaggerEndpoint(EndpointUrl, EndpointName); });
        }
    }
}
