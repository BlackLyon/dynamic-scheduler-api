﻿using System;

namespace DinamycScheduler.Model
{
    public class Turn
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Hour { get; set; }
    }
}
