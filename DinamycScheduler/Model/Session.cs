using System;

namespace DinamycScheduler.Model {

    public class Session {

        public int Id { get; set; }
        public TimeSpan InitTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public string PatientName { get; set; }

    }
}