
namespace DinamycScheduler.Model 
{
    public class Time 
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
    }
}