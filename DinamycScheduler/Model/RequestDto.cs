
namespace DinamycScheduler.Model {

    public class RequestDto {

        public int Id { get; set; }
        public Time Hour { get; set; }

    }

}