﻿
namespace DinamycScheduler.Model
{
    public class Credentials
    {
        public string DNI { get; set; }
        public string Password { get; set; }
    }
}
