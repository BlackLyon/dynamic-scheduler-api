﻿
using DinamycScheduler.Model;

namespace DinamycScheduler.Repository
{
    public interface IUserRepository
    {
        User Login(string dni, string password);
    }
}

