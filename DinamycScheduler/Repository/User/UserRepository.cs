﻿using DinamycScheduler.Model;

namespace DinamycScheduler.Repository
{
    public class UserRepository: IUserRepository
    {
        private readonly ISqlConnector sqlConnector;
        public UserRepository(ISqlConnector sqlConnector)
        {
            this.sqlConnector = sqlConnector;
        }   

        public User Login(string dni, string password)
        {
            var userId = LoginUser(dni, password);
            return GetUser(userId, userId > 22);
        }

        private int LoginUser(string dni, string password) {
            var query = $"select tb_login.id from turnerodinamico.tb_login where tb_login.dni = {dni} and tb_login.pass = '{password}';";
            var reader = sqlConnector.ExecuteQueryDict(query);
            if (reader is null) return -1;

            return (int)reader["id"];
        }

        private User GetUser(int userId, bool isDoctor) {
            var patientQuery = $"select paciente.id, paciente.nombre, paciente.apellido from turnerodinamico.paciente where paciente.id = {userId};";
            var doctorQuery = $"select pmedico.id, pmedico.nombre, pmedico.apellido from turnerodinamico.pmedico where pmedico.id = {userId};";
            var query = isDoctor ? doctorQuery : patientQuery;
            var reader = sqlConnector.ExecuteQueryDict(query);
            if (reader is null) return null;

            return new User() { 
                Id = (int)reader["id"],
                Name = reader["nombre"].ToString(),
                LastName = reader["apellido"].ToString(),
                Type = (isDoctor) ? 1 : 0
            };
        }
    }
}
