﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace DinamycScheduler.Repository
{
    public class SqlConnector : ISqlConnector
    {
        private MySqlConnection _connection;
        public SqlConnector(IConfiguration appSettings)
        {
            _connection = new MySqlConnection(appSettings.GetConnectionString("DBConexion"));
        }

        public MySqlDataReader ExecuteQuery(string query)
        {
            try
            {
                _connection.Open();
                using MySqlCommand cmd = new MySqlCommand(query, _connection);
                var dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows && dataReader.Read())
                    return dataReader;

                return null;
            }
            catch (MySqlException)
            {
                return null;
            }
            finally
            {
                _connection.Close();
            }
        }

        public Dictionary<string, object> ExecuteQueryDict(string query)
        {
            try
            {
                _connection.Open();
                using MySqlCommand cmd = new MySqlCommand(query, _connection);
                var dataReader = cmd.ExecuteReader();

                if (dataReader.HasRows)
                    return GetValues(dataReader);

                return null;
            }
            catch (MySqlException)
            {
                return null;
            }
            finally
            {
                _connection.Close();
            }
        }

        private Dictionary<string, object> GetValues(MySqlDataReader reader)
        {
            var dict = new Dictionary<string, object>();
            var colums = GetColumnNames(reader);
            while (reader.Read())
            { 
                foreach (var column in colums)
                {
                    if (!dict.ContainsKey(column))
                    {
                        dict.Add(column, reader[column]);
                    }
                }
            }
            return dict;
        }

        private string[] GetColumnNames(MySqlDataReader reader)
        {
            var fieldCount = reader.FieldCount;
            var arrayColumns = new string[fieldCount];
            for (int i = 0; i < fieldCount; i++)
            {
                arrayColumns[i] = reader.GetName(i);
            }
            return arrayColumns;
        }

        public int ExecuteNonQuery(string query)
        {
            try
            {
                _connection.Open();
                using MySqlCommand cmd = new MySqlCommand(query, _connection);
                return cmd.ExecuteNonQuery();
            }
            catch (MySqlException)
            {
                return -1;
            }
            finally
            {
                _connection.Close();
            }
        }
    }
}
