﻿using System;
using DinamycScheduler.Model;

namespace DinamycScheduler.Repository.Doctor
{
    public interface IDoctorRepository
    {
        public Session AcceptNextTurn(int doctorId, TimeSpan fromHour);
        public TimeSpan ExtendSession(int sessionId);
        public bool EndSession(int sessionId, TimeSpan endTime);
        public Session AcceptEmergencyTurn(int doctorId, TimeSpan startTime);
    }
}
