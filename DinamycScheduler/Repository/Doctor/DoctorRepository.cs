﻿using System;
using DinamycScheduler.Model;

namespace DinamycScheduler.Repository.Doctor
{
    public class DoctorRepository : IDoctorRepository
    {
        private readonly ISqlConnector sqlConnector;

        public DoctorRepository(ISqlConnector sqlConnector)
        {
            this.sqlConnector = sqlConnector;
        }

        public Session AcceptNextTurn(int doctorId, TimeSpan fromHour)
        {
            var insertQuery = new QueryBuilder()
                .Insert("turnerodinamico.consulta (id, idTurno, horarioInicio,idPaciente, horaFinEstimada, idPersonalMedico)")
                .Select($"(id_consulta.id_consulta + 1) AS id_consulta, id AS id_turno, '{fromHour.Format()}', idPaciente, addtime('{fromHour.Format()}', '00:15:00') AS horaFinEstimada, idPersonalMedico")
                .From("turnerodinamico.turno turno")
                .Join("LEFT", "(select max(id) id_consulta FROM turnerodinamico.consulta) AS id_consulta")
                .On("1 = 1")
                .Where($"turno.idPersonalMedico = {doctorId}")
                .And("turno.estadoTurno = 'activo'")
                .Order("hora", asc: true)
                .Limit(1)
                .Build();
            var result = sqlConnector.ExecuteNonQuery(insertQuery);
            return (result <= 0) ? null : GetLastSession();
        }

        public TimeSpan ExtendSession(int sessionId)
        {
            var selectQuery = new QueryBuilder()
                .Select("horaFinEstimada")
                .From("turnerodinamico.consulta")
                .Where($"id={sessionId}") 
                .Build();

            var reader = sqlConnector.ExecuteQueryDict(selectQuery);
            if (reader is null) return TimeSpan.Zero;

            var query = $"update turnerodinamico.consulta set horaFinEstimada = addtime(horaFinEstimada, '00:05:00') where id = {sessionId}";

            var result = sqlConnector.ExecuteNonQuery(query);
            if (result == -1) return TimeSpan.Zero;

            ExtendNextsSessions(sessionId);

            var reader2 = sqlConnector.ExecuteQueryDict(selectQuery);
            return (TimeSpan)reader2["horaFinEstimada"];
        }

        public bool EndSession(int sessionId, TimeSpan endTime)
        {
            return SaveSessionEndTime(sessionId, endTime) &&
                EndSession(sessionId) &&
                UpdateSessionHour(sessionId);
        }

        public Session AcceptEmergencyTurn(int doctorId, TimeSpan startTime)
        {
            var query = new QueryBuilder()
                .Insert("turnerodinamico.consulta (id, horarioInicio, horaFinEstimada,idPersonalMedico)")
                .Select($"(max(id) + 1) AS id_consulta, '{startTime.Format()}', addtime('{startTime.Format()}', '00:15:00') AS horaFinEstimada, {doctorId} ")
                .From("turnerodinamico.consulta")
                .Order("id", asc: true)
                .Limit(1)
                .Build();
            if (sqlConnector.ExecuteNonQuery(query) <= 0) return null;

            var session = GetLastSession();
            ExtendNextSessionsByEmergency(session.Id);
            return session;
        }

        private void ExtendNextsSessions(int sessionId)
        {
            var query = new QueryBuilder()
                .Update("turnerodinamico.turno")
                .Join("INNER", "turnerodinamico.consulta")
                .On($"consulta.id = {sessionId}")
                .And("horarioInicio < hora").And("consulta.idPersonalMedico = turno.idPersonalMedico").And("turno.estadoTurno= 'activo'")
                .Set("hora = addtime(hora, '00:05:00')")
                .Build();
             sqlConnector.ExecuteNonQuery(query);
         }

        private void ExtendNextSessionsByEmergency(int sessionId)
         {
             var query = new QueryBuilder()
                 .Update("turnerodinamico.turno")
                 .Join("INNER", "turnerodinamico.consulta")
                 .On($"consulta.id = {sessionId}")
                 .And("horarioInicio <= hora").And("consulta.idPersonalMedico = turno.idPersonalMedico").And("turno.estadoTurno = 'activo'")
                 .Set("hora = addtime(hora, '00:15:00')")
                 .Build();
            sqlConnector.ExecuteNonQuery(query);
        }

        private Session GetLastSession() {
            var selectQuery = new QueryBuilder()
                .Select("consulta.id, consulta.horarioInicio, consulta.horaFinEstimada, paciente.nombre, paciente.apellido")
                .From("turnerodinamico.consulta")
                .Join("LEFT", "paciente on consulta.idPaciente = paciente.id")
                .Where("consulta.id = (select max(id) from turnerodinamico.consulta)")
                .Limit(1)
                .Build();

            var reader = sqlConnector.ExecuteQueryDict(selectQuery);
            if (reader is null) return null;
            return new Session()
            {
                Id = (int)reader["id"],
                InitTime = (TimeSpan)reader["horarioInicio"],
                EndTime = (TimeSpan)reader["horaFinEstimada"],
                PatientName = $"{reader["nombre"]} {reader["apellido"]}"
            };
        }

        private bool EndSession(int sessionId)
        {
            var query = new QueryBuilder()
                .Update("turnerodinamico.turno")
                    .Join("INNER", "turnerodinamico.consulta")
                        .On($"consulta.id = {sessionId}")
                            .And("consulta.idTurno = turno.id")
                            .And("turno.estadoTurno = 'activo'")
                    .Set("turno.estadoTurno = 'finalizado'")
                .Build();
            return sqlConnector.ExecuteNonQuery(query) >= 1;
        }

        private bool UpdateSessionHour(int sessionId)
        {
            var query = new QueryBuilder()
                .Update("turnerodinamico.turno")
                    .Join("INNER", "turnerodinamico.consulta")
                        .On($"consulta.id = {sessionId}")
                            .And("horarioInicio < hora")
                            .And("consulta.idPersonalMedico = turno.idPersonalMedico")
                            .And("turno.estadoTurno= 'activo'")
                    .Set("turno.hora = addtime(turno.hora, timediff(consulta.horarioFin, consulta.horaFinEstimada))")
                .Build();
            return sqlConnector.ExecuteNonQuery(query) >= 1;
        }

        private bool SaveSessionEndTime(int sessionId, TimeSpan endTime)
        {
            var query = new QueryBuilder()
                .Update("turnerodinamico.consulta")
                    .Set($"horarioFin = '{endTime.Format()}'")
                    .Where($"id = {sessionId};")
                .Build();
            return sqlConnector.ExecuteNonQuery(query) >= 1;
        }
    }
}
