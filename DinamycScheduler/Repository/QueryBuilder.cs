﻿using System;
namespace DinamycScheduler.Repository
{
    internal class QueryBuilder
    {
        private string query = "";
        private int lastCalled = 0;

        public QueryBuilder Select(string param)
        {
            lastCalled = 1;
            query += $"SELECT {param} ";
            return this;
        }
        public QueryBuilder Update(string param)
        {
            query += $"UPDATE {param} ";
            return this;
        }
        public QueryBuilder Insert(string param)
        {
            query += $"INSERT INTO {param} ";
            return this;
        }
        public QueryBuilder From(string param)
        {
            if (lastCalled != 1)
                throw new InvalidOperationException("from clausure must always be called after a select");

            query += $"FROM {param} ";
            return this;
        }
        public QueryBuilder Where(string param)
        {
            query += $"WHERE {param} ";
            return this;
        }
        public QueryBuilder Limit(int limit)
        {
            query += $"LIMIT {limit} ";
            return this;
        }
        public QueryBuilder Join(string side, string param)
        {
            query += $"{side} JOIN {param} ";
            return this;
        }
        public QueryBuilder And(string param)
        {
            query += $"AND {param} ";
            return this;
        }
        public QueryBuilder On(string param)
        {
            query += $"ON {param} ";
            return this;
        }
        public QueryBuilder Set(string param)
        {
            query += $"SET {param} ";
            return this;
        }

        public QueryBuilder With(string param)
        {
            query += $"WITH {param} ";
            return this;
        }

        public QueryBuilder Start()
        {
            query += "(";
            return this;
        }

        public QueryBuilder End()
        {
            query += ") ";
            return this;
        }

        public QueryBuilder Continue(string param)
        {
            query += $"{param} ";
            return this;
        }

        public QueryBuilder Order(string param, bool asc)
        {
            var ascQuery = asc ? "ASC" : "DESC";
            query += $"ORDER BY {param} {ascQuery} ";
            return this;
        }
        public string Build()
        {
            return query;
        }
    }
}
