﻿using System;
using DinamycScheduler.Model;

namespace DinamycScheduler.Repository.Patient
{
    public class PatientRepository : IPatientRepository
    {
        private readonly ISqlConnector sqlConnector;
        public PatientRepository(ISqlConnector sqlConnector)
        {
            this.sqlConnector = sqlConnector;
        }

        public Turn GetAssignedTurn(int userId)
        {
            var query = new QueryBuilder()
                .Select("turno.id, turno.fecha, turno.hora")
                .From("turnerodinamico.turno")
                .Where($"turno.idPaciente= {userId}")
                .And("turno.estadoTurno='activo'")
                .Limit(1)
                .Build();
            var reader = sqlConnector.ExecuteQueryDict(query);
            if (reader is null) return null;

            return new Turn()
            {
                Id = (int)reader["id"],
                Date = (DateTime)reader["fecha"],
                Hour = (TimeSpan)reader["hora"]
            };
        }

        public TimeSpan CancelTurnAndAssignToNext(int userId)
        {
            var availableTurn = FindAvailableTurn(userId);
            return (CancelTurn(userId) >= 0) ? availableTurn : TimeSpan.Zero;
        }

        public bool AcceptNextAvailableTurn(int userId, TimeSpan turnHour)
        {
            var query = new QueryBuilder()
                .Update("turnerodinamico.turno")
                .Set($"turno.hora = '{turnHour}'")
                .Where($"idPaciente = {userId}")
                .Build();
            var result = sqlConnector.ExecuteNonQuery(query);
            return (result >= 1);
        }

        public TimeSpan GetAvailableTurnHour(int userId)
        {
            var query = new QueryBuilder()
                .With("ultimo_id_activo AS")
                    .Start()
                        .Select("CASE WHEN estadoTurno = 'activo' THEN idPaciente ELSE 'noTurno' END idPacienteUltimoTurno")
                        .From("turnerodinamico.turno")
                        .Order("hora", asc: false)
                        .Limit(1)
                    .End()
                .Continue(", datos_cancelacion AS")
                    .Start()
                        .Select("hora, idPaciente idPacienteCancelacion")
                        .From("turnerodinamico.turno")
                        .Where("estadoTurno = 'cancelado'")
                    .End()
                .Select($"CASE WHEN idPacienteUltimoTurno != idPacienteCancelacion AND idPacienteUltimoTurno = {userId} THEN hora ELSE NULL END AS turnoDisponible")
                .From("ultimo_id_activo")
                .Join("", "datos_cancelacion")
                .Build();

            var reader = sqlConnector.ExecuteQueryDict(query);

            if (reader is null || string.IsNullOrWhiteSpace(reader["turnoDisponible"]?.ToString()))
                return TimeSpan.Zero;

            return (TimeSpan)reader["turnoDisponible"];
        }

        private int FindPacientAvailableForTurn()
        {
            var query = $"select case when estadoTurno = 'activo' then idPaciente else 'noTurno' end ultimoTurno from turnerodinamico.turno order by hora desc limit 1;";
            var reader = sqlConnector.ExecuteQueryDict(query);
            return (reader is null) ? -1 : (int)reader["ultimoTurno"];
        }

        private TimeSpan FindAvailableTurn(int userId)
        {
            var availableTurnQuery = new QueryBuilder()
                .Select("turno.hora")
                .From("turnerodinamico.turno")
                .Where($"turno.idPaciente = {userId}")
                .And("turno.estadoTurno='activo'")
                .Limit(1)
                .Build();
            var reader = sqlConnector.ExecuteQueryDict(availableTurnQuery);
            return (reader is null) ? TimeSpan.Zero : (TimeSpan)reader["hora"];
        }

        private int CancelTurn(int userId)
        {
            var cancelTurnQuery = new QueryBuilder()
                .Update("turnerodinamico.turno")
                .Set("estadoTurno = 'cancelado'")
                .Where($"turno.idPaciente = {userId}")
                .And("turno.estadoTurno = 'activo'")
                .Build();
            return sqlConnector.ExecuteNonQuery(cancelTurnQuery);
        }

    }
}
