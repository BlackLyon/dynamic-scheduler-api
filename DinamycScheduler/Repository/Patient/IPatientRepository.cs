﻿using System;
using DinamycScheduler.Model;

namespace DinamycScheduler.Repository.Patient
{
    public interface IPatientRepository
    {
        Turn GetAssignedTurn(int userId);
        TimeSpan CancelTurnAndAssignToNext(int userId);
        bool AcceptNextAvailableTurn(int userId, TimeSpan turnHour);
        TimeSpan GetAvailableTurnHour(int userId);
    }
}
