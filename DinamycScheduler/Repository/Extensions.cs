﻿
using System;

namespace DinamycScheduler.Repository
{
    public static class Extensions
    {
        public static string Format(this TimeSpan timeSpan) =>
            $"{timeSpan.Hours}:{timeSpan.Minutes}:{timeSpan.Seconds}";
    }
}
