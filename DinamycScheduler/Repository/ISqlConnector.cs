﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace DinamycScheduler.Repository
{
    public interface ISqlConnector
    {
        Dictionary<string, object> ExecuteQueryDict(string query);

        MySqlDataReader ExecuteQuery(string query);

        int ExecuteNonQuery(string query);
    }
}
